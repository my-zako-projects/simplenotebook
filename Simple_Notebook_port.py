import os
from time import sleep

storage_path = os.path.dirname(__file__) + '\\'

def file_exists(title):
    return os.path.exists(f'{storage_path}{title}.txt')

def new_note(title):
    with open(f'{storage_path}{title}.txt', 'w') as file:
        write_in_file(file)

def write_in_file(file):
    print('Write your note. To end press enter 2 times.')
    while True:
        line = input()
        if not line:
            break
        file.write(line + '\n')

def read_file(title):
    with open(f'{storage_path}{title}.txt', 'r') as file:
        print(file.read())

def add_to_file(title):
    with open(f'{storage_path}{title}.txt', 'a') as file:
        write_in_file(file)
def commands_print(dict):
    print('---COMMANDS---')
    for key, value in dict.items():
        print(key + ':', value)

def rename(old_name):
    new_name = input('Type a new title for a file: > ')
    os.rename(storage_path + old_name + '.txt', storage_path + new_name + '.txt')


def main_program():
    commands_descriprion_dictionary = {'NEW': 'creates a new note',
                                       'NOTE': 'chooses or changes a note to work with',
                                       'READ': 'displays text of the note',
                                       'ADD': 'complements an existing note',
                                       'DELETE': 'deletes a note',
                                       'REWRITE': 'clears the note to rewrite a new text',
                                       'SHOW': 'shows notes you have',
                                       'RENAME': 'renames a note'
                                       }
    active_note = ''
    print("""Hello! Welcome to Simple Notebook.
You can type HELP to view list of commands. To quit press Enter.""")
    while True:
        command = input('What do you want to do? > ')
        if not command:
            print('Walk blessed!')
            sleep(1)
            break
        elif command.upper() == 'HELP':
            commands_print(commands_descriprion_dictionary)
        elif command.upper() == 'NOTE':
            if active_note:
                print(f'Current note: {active_note}')
            active_note = input('Enter note title to work with (Press Enter to go back): > ')
            while active_note and not file_exists(active_note.lower()):
                active_note = input('This file doesn\'t exist! Enter a valid name: > ')
            if active_note:
                print(f'Current note: {active_note}')
        elif command.upper() == 'NEW':
            title = input('Enter note title: > ')
            while file_exists(title.lower()):
                title = input('Such a file already exists! Enter a different name: > ')
            new_note(title)
        elif command.upper() == 'READ':
            if active_note:
                read_file(active_note)
            else:
                title = input('Enter a file name to work with: > ')
                while title and not file_exists(title.lower()):
                    title = input('This file doesn\'t exist! Enter a valid name: > ')
                if title:
                    read_file(title)
        elif command.upper() == 'ADD':
            if active_note:
                add_to_file(active_note)
            else:
                title = input('Enter a file name to work with: > ')
                while title and not file_exists(title.lower()):
                    title = input('This file doesn\'t exist! Enter a valid name: > ')
                if title:
                    add_to_file(title)
        elif command.upper() == 'REWRITE':
            if active_note:
                new_note(active_note)
            else:
                title = input('Enter a file name to work with: > ')
                while title and not file_exists(title.lower()):
                    title = input('No file found! Enter a valid name: > ')
                if title:
                    new_note(title)
        elif command.upper() == 'DELETE':
            if active_note:
                os.remove(f'{storage_path}{active_note}.txt')
                active_note = ''
            else:
                title = input('Enter a file name to work with: > ')
                while title and not file_exists(title.lower()):
                    title = input('No file found! Enter a valid name: > ')
                if title:
                    os.remove(f'{storage_path}{title}.txt')
        elif command.upper() == 'SHOW':
            for file_name in os.listdir(storage_path):
                if file_name.endswith('.txt'):
                    print(file_name.split('.')[0])
        elif command.upper() == 'RENAME':
            if active_note:
                rename(active_note)
                active_note = ''
            else:
                title = input('Enter a file name to work with: > ')
                while title and not file_exists(title.lower()):
                    title = input('No file found! Enter a valid name: > ')
                if title:
                    rename(title)

        else:
            print("""Sorry, there is no such command.
You can type HELP to view list of commands.""")


main_program()


